# Intro

Personal blog for Dr Piotr Gryko

## Build Instructions:


```shell
sudo apt-get install ruby ruby-dev make build-essential

sudo gem install bundler jekyll

bundle install

bundle exec jekyll b

bundle exec jekyll s

```

or via docker

```shell
docker run -it --rm \
    --volume="$PWD:/srv/jekyll" \
    -p 4000:4000 jekyll/jekyll \
    jekyll serve
```

## Generating subsource integrity hashes for css and js files

```console
$ cat highlightjs.piperita.scss | openssl dgst -sha384 -binary | openssl enc -base64 -A

OB6RhmazgXzOVpTtSZznaIjQtE32yI73u20f3CZF9Bx8CYgcWE+quTt74oa91Auk
```

# Terminal SVG animations

Terminal SVG animations are generated using https://github.com/nbedos/termtosvg

The window frame template is used

```shell
termtosvg -t window_frame
```


## Hosting
This source code is hosted on [gitlab](https://gitlab.com/pgryko/piotr-gryko) and automatically deployed onto the web using [Netlify](https://netlify.com)

## Theme
Theme is forked from [jekyll-theme-chirpy](https://github.com/cotes2020/jekyll-theme-chirpy),

## License
MIT

