---
title: Notes on Diffusion models
categories: [Python, Machine learning, AI, Tokenizer]
tags: 
  - Python
  - Machine learning
  - AIs
pin: false
math: true
---

#  Intro:

Key idea of diffion models is to train a model to remove (Gausian) noise from an image step by step.

![“Denoising Diffusion Probabilistic Models”](/assets/img/posts/2024-05-21-notes-diffusion-models/denoising_diffusion_probabilistic_models_graph.webp)*Figure: Denoising Diffusion Probabilistic Models*

![“Denoise process”](/assets/img/posts/2024-05-21-notes-diffusion-models/denoise.gif)*Illustration of the reverse process of a diffusion model. The model starts from a complete noise image, and then, step by step, it removes the noise until it gets a clean image.*


The notation \( X_0 \sim q(X_0) \) you provided in the image is used in statistics and probability theory. It suggests that the random variable \( X_0 \) follows the distribution \( q \), which is defined by the probability density or mass function \( q(X_0) \). This means that \( X_0 \) is a random variable whose behavior (how it spreads out, what values it is likely to take, etc.) is described by the function \( q \).

In practical terms, this is a way to state that when you observe or simulate values of \( X_0 \), they come from the distribution described by \( q \). This kind of notation is common in contexts like Bayesian statistics, stochastic modeling, and Monte Carlo simulation methods, where defining the distribution of random variables explicitly is crucial for analysis and computations.