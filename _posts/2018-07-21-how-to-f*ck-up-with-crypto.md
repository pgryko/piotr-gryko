---
layout:     post
title:      How to f*ck up with crypto currencies
date:       2018-07-21 12:32:18
summary:    Loosing your wallet, getting hacked, falling for a scam, sending money to a wrong address - there are many ways to mess up with cryptocurrencies.
categories: [Cryptocurrencies, Blockchain, Bitcoin]
thumbnail: jekyll
tags:
 - Cryptocurrencies
 - Blockchain
 - Bitcoin
---


Loosing your wallet, getting hacked, falling for a scam, sending money to a wrong address - there are many ways to mess up with cryptocurrencies. While the Bitcoin protocol is quite well established (it's been around for 10yrs), the surrounding software (wallets, exchanges and payment systems etc) are all beta software at best, and general a nightmare to use. Here are 10 ways in which you can mess up with cryptocurrencies, with recommendations on how to avoid them.

This is a summary of a presentation I used to give prior to the 2017 crypto bubble. Original presentation can be found here: <https://docs.google.com/presentation/d/1ki63xerzbnnEpbeLkOTBmJ6_qiN3-O7_L0ToBn0xl3c/>

## 1. Gambling what you can't afford to loose

Cryptocurrencies are NOT and investment. There are no laws governing them, or guarantees on issues to honour promises. They are an extremely early technology, with volatile pricing, driven by pure speculation. At best they are speculation and at worst gambling.

## 2. "Hacked coin-exchange"

Many online coin exchanges start up, accept deposits, run for a while and then get "hacked". I.e. They run off with your money and disappear into the ether. 

The most famous example of this is MTGOX, who ran away with xx Bitcoin

Even more established exchanges, I.e. ones that have been around for more than 2 years, are constantly under attack by hackers.

Tips: Try to use exchanges that have been around for longer, enable two stage authentication and if you have large amounts of cryptocurrency, keep if offline on a cold wallet

## 3. Getting hacked yourself

Be careful on what you install on your computer. Be especially careful of what coin wallets you use. Try to use official ones.

Protip: Use a password manager to store unique passwords for each site and service that you use. Websites are supposed to salt and hash all user passwords, (think of it like washing your hands after going to the toilet), but many don't. Special thanks goes to Linkdin for leaking my password in 2016

## 4. Sending money to a wrong address
Double-check the address you are sending money to. If you send money to a wrong address, it's gone forever. There is no way to get it back. If your sending large amounts of money, send a small amount first, and check that it arrives.

## 5. Getting locked out of your wallet/losing your wallet/forgetting your password

If you loose your wallet, or forget your password, you can't get your money back. There is no way to reset your password.
Store extra copies of your encrypted wallet in a safe place offline. Test to make sure that you can recover your wallet from the backup. Keep the password to your wallet in a safe place and seperate to the wallet itself

## 6. Buying a coin with no whitepaper

Many coins are just scams. They have no whitepaper, no code, no team, no nothing. They are just a website with a promise. Don't buy them. A whitepaper is a document that describes the technology behind the coin, and how it works. It should be written in a way that is understandable to a technical person.

## 7. Toilet paper as a white paper

The while paper should be of a high technical standard, clear to read for a technical user. It should describe both the strengths and limitations of the current technology. Take a look at the Bitcoin and Ethereum whitepapers as good examples.

## 8. No source code / Closed source

A trust-less peer-to-peer system requires transparent code. An SDK only, does not constitute open source (e.g. Ripple)
Look for Github/Gitlab project pages. How often are the projects updated e.g. (Ethereum VS DodgeCoin). Activity on the project page is a good indicator of how active the project is.

## 9. Premined coins

Some coins will pre-mine lots of coins and then dump and sell them once the coin becomes popular

## 10. Pump and dump

Some coins will be pumped and dumped by their creators. They will buy lots of coins, pump the price, and then sell them all at once, crashing the price. This is especially true for coins that are only traded on a single exchange.

## 11. Getting murdered

If you have a lot of money in cryptocurrencies, you may become a target for criminals. Be careful who you tell about your wealth. Don't tell people how much you have.
![5 dollar wrentch](/assets/img/posts/2018-07-21-how-to-f*ck-up-with-crypto/xkcd_security.png){: width="972" height="589" }

## 12. Listening to Morons

![Morons](/assets/img/posts/2018-07-21-how-to-f*ck-up-with-crypto/moron_speaking.webp){: width="972" height="589" }
