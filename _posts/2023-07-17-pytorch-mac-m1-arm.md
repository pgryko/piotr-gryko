---
title: Setting up PyTorch Development for Mac M1/M2 ARM
categories: [Python, Machine learning, Docker, Mac, ARM]
tags: 
  - Python
  - Machine learning
  - Docker
pin: false
---

Want to build pytorch on an M1 mac? Running into issues with the build process? This guide will help you get started.

There are issues with building PyTorch on Mac M1/M2 ARM devices due to [conflicts with protobuf](https://github.com/pytorch/pytorch/issues/82831) that comes with OSX 12 and 13.

The following instructions are based off [the pytorch official guide](https://github.com/pytorch/pytorch#from-source):

```shell
# create a conda environment
conda create -n pytorch-build python=3.10
conda activate pytorch-build

conda install astunparse numpy ninja pyyaml setuptools cmake cffi \
 typing_extensions future six requests dataclasses

# These are useful for development
conda expecttest hypothesis mypy pytest ninja

git clone --recursive https://github.com/pytorch/pytorch
```

Params do the following:

BUILD_CAFFE2=0:  skip unnecessary build

and USE_ONNX=0: USE_ONNX=0 prevents trying to build protobuf

REL_WITH_DEB_INFO=1: include debugging symbols.

MACOSX_DEPLOYMENT_TARGET=12.3: target a macOS that has MPS available

If your planning on developing, [enabling caching](https://github.com/pytorch/pytorch/blob/main/CONTRIBUTING.md#make-no-op-build-fast) will speed up the build process (after the first build):

```shell
brew install ccache
# increase max size of cache
ccache -M 25Gi  # -M 0 for unlimited
# unlimited number of files
ccache -F 0

export CMAKE_C_COMPILER_LAUNCHER=ccache
export CMAKE_CXX_COMPILER_LAUNCHER=ccache
export CMAKE_CUDA_COMPILER_LAUNCHER=ccache
```

Finally, build!

```shell
export CMAKE_PREFIX_PATH=${CONDA_PREFIX:-"$(dirname $(which conda))/../"}
MACOSX_DEPLOYMENT_TARGET=12.3 BUILD_CAFFE2=0 USE_ONNX=0 REL_WITH_DEB_INFO=1 python setup.py develop --cmake
```

and install

```shell
pip install -e .
```

and confirm that it works:

```shell
import torch

device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
print(f"Using {device} device")

x = torch.rand(5, 3)
print(x)
```

Many thanks to [@damian0815](https://medium.com/@damian0815/how-to-debug-native-mps-operations-in-pytorch-f094b7f4e8f0) for the MPS build tip.