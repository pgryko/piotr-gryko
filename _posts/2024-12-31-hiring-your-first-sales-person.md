---
title: When (and How) to Hire Your First Salesperson - A Founder's Guide
categories: [Business, Sales, Startups]
tags:
  - Sales Hiring
  - Startup Growth
  - Product Market Fit
  - Sales Strategy
  - Founder Advice
pin: false
math: false
image:
  path: /assets/img/posts/2024-12-31-hiring-your-first-sales-person/hiring-your-first-sales-person.webp
  alt: Home GPU setup in a Cubist style
---


## The Foundation First
Before you even think about hiring a salesperson, there's one critical truth you need to accept: **you must establish market validation for your product**. Salespeople need something concrete to sell, not just promises and PowerPoint decks.

## The Product Must Be Ready
Your product should be:
- Clearly defined
- Well-packaged
- Repeatable in its sales motion

## Warning: The Danger of Premature Sales Hiring
When salespeople lack a solid product, they often resort to:
- "Selling the deck"
- Making promises they can't keep
- Creating fictional features

This leads to two painful outcomes:
1. Unwinding deals and losing revenue
2. Compromising your product roadmap to meet unrealistic promises

## The Founder's Role
> "If you can't define it well enough to sell it to a customer, then nobody can."

As a founder, you **must** be able to:
- Sell your product effectively
- Define your target market
- Understand who wants to buy it

*Bonus question:* How can you convince investors if you can't convince customers?

## The Right Way to Onboard Your First Sales Hire

### 1. Create a Strong Partnership
- Make them your commercial lead
- Position yourself as their sales engineer
- Stay involved without micromanaging

### 2. Evolution of Roles
Graduate to being a "closer":
- Let your salesperson set up opportunities
- Step in for the final close when needed

## Aligning Incentives
Remember:
- Tie compensation to company goals
- Include strategic objectives, not just revenue
- Understand that good salespeople optimize for reward/effort ratio
- Use this to your advantage

## The Money Question
Important rules about compensation:
- Pay market rate or don't hire
- Avoid "budget" salespeople
- If you can't afford top talent:
  - Keep selling yourself
  - Raise capital once you have product-market fit

## Required Reading
📚 **Essential Resource**: "The Hard Thing About Hard Things" by Ben Horowitz
- Focus on the section about hiring the first head of sales
- Read it multiple times until the lessons stick

---
*Remember: A great salesperson can accelerate your success, but only if you've laid the proper groundwork first.*

<!-- Don’t hire a salesperson until you have established that there is a market for your product. To be effective, salespeople need a product to sell. Ideally, it should be packaged simply and well enough that they can execute the same sales motion over and over again.

Sales people should not be “selling the deck”, or making shit up. You need to have things figured out well enough that your salespeople never have an original thought in front of a customer. Otherwise, if you push them to close deals with nonexistent product, they will just lie and make shit up to close a deal. Now, you either have to unwind the deal and lose the revenue, or else you need to sacrifice your road map.

Some founder needs to be able to sell your product. If you can’t define it well enough to sell it to a customer, and well enough to figure out who wants to buy it, then nobody can. And nobody will. Bonus: how on earth will you sell it to investors if you can’t sell it to customers?

When you DO get a sales person, make sure that your first sales hire is conjoined at the hip with you. They are now the commercial lead when talking to a customer, and you are their sales engineer. This keeps you in the deal without micromanaging the salesperson, which is nearly always a bad idea.

Graduate to being a “closer”. Get your first salesperson to line up the shot for you, and then pull you in when it’s time to take the kill shot.

Tie the salesperson’s success to the goals of the company. Not just revenue goals, but achieving strategic objectives. Good commercial people are lazy, as they are trying to minimize effort for maximal reward. That’s literally why we hire them. So incentivize them to be productive at things you want done.

Hire a salesperson when you can afford to PAY them. You DON’T want a cheap salesperson in a startup. If you can’t afford to pay market rate for proven skill, do it your damn self until you can afford it. If you can’t afford it on cash flow, then raise money for sales and marketing once you have established product market fit.

Read “The Hard Thing About Hard Things”, especially the section where Ben Horowitz talks about hiring his first head of sales. Keep reading it until it sinks in. -->